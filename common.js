"use strict";

/*Завдання
Створити об’єкт “студент” та проаналізувати його табель. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Створити порожній об’єкт student, з полями name та lastName.
Запитати у користувача ім’я та прізвище студента, отримані значення записати у відповідні поля об’єкта.
У циклі запитувати у користувача назву предмета та оцінку щодо нього. Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл. Записати оцінки з усіх предметів як студента tabel.
порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає, вивести повідомлення “Студент переведено на наступний курс”.
Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення Студенту призначено стипендію.*/

function studentData() {
  const student = {
    name: '',
    lastName: '',
    tabel: {}
  };

  const name = prompt("Введіть Ваше ім'я");
  if (name === null || name === undefined|| name === "") {
    alert('Помилка');
    return null;
  }
  student.name = name;

  const lastName = prompt("Введіть Ваше прізвище");
  if (lastName === null || lastName === undefined || lastName === "") {
    alert('Помилка');
    return null;
  }
  student.lastName = lastName;

  while (true) {
    const subject = prompt("Який предмет здано?");
    if (subject === null) {
      break;
    } 

    student.tabel[subject] = +prompt("На яку оцінку?");
  }
  let badMarks = 0;

  for (let key in student.tabel) {
    if (student.tabel[key] < 4) {
      badMarks++;
    }
  }

  badMarks ? alert(`Кількість поганих оцінок: ${badMarks}`) : alert("Студент переведено на наступний курс");

    let averageMark = 0;
    let courseCount = 0;

    for (let key in student.tabel) {
      courseCount++;
      averageMark += student.tabel[key];
    }

    const averageMarkSum = averageMark / courseCount;

    averageMarkSum > 7 ? alert("Студенту призначено стипендію") : null;

  return student;
}

const student = studentData();